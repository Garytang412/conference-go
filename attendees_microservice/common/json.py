from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet

class QuerySetEncoder(JSONEncoder):
    def default(self,obj):
        if isinstance(obj,QuerySet):
            return list(obj)
        else:
            return super().default(obj)



class DateEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj,datetime):
            return datetime.isoformat(obj)
        else:
            return super().default(obj)


class ModelEncoder(DateEncoder,QuerySetEncoder,JSONEncoder):
    encoders = {}
    def default(self, obj):
        if isinstance(obj,self.model):
            result = {}
            if hasattr(obj,'get_api_url') and "href" in self.properties:
                result['href'] = obj.get_api_url()
            for i in self.properties:
                if i == 'href':
                    continue

                if i in self.encoders:
                    result[i] = self.encoders[i].default(getattr(obj,i))
                else:
                    result[i] = getattr(obj,i)
                    
            # escape hatch
            result.update(self.get_extra_data(obj))
            return result
        else:
            return super().default(obj)
        
    # escape hatch 
    def get_extra_data(self,obj):
        return {}