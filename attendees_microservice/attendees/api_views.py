from django.http import JsonResponse
from attendees.encoders import AttendeeListEncoder, AttendeeDetailEncoder
from .models import Attendee, ConferenceVO
# from events.models import Conference
from django.views.decorators.http import require_http_methods
import json


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees}, encoder=AttendeeListEncoder, safe=False
        )
    else:
        # POST create new attendee:
        attendee = json.loads(request.body)
        print(attendee)
        try:
            conference_href = f"/api/conferences/{conference_vo_id}/"
            print(conference_href)
            attendee["conference"] = ConferenceVO.objects.get(import_href=conference_href)
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Conference Id Invalid"}, status=400
            )

        new_attendee = Attendee.objects.create(**attendee)
        return JsonResponse(
            new_attendee, encoder=AttendeeDetailEncoder, safe=False
        )


# GET detail ,PUT update ,DELETE
@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_attendee(request, pk):
    if request.method == "GET":
        try:
            attendee = Attendee.objects.get(id=pk)
            return JsonResponse(
                attendee, encoder=AttendeeDetailEncoder, safe=False
            )
        except Attendee.DoesNotExist:
            return JsonResponse({"message": "Invalid Attendee ID"}, status=400)

    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        # PUT -> update
        attendee = json.loads(request.body)
        try:
            attendee["conference"] = ConferenceVO.objects.get(id=pk)
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Conference Id Invalid"}, status=400
            )

        new_attendee = Attendee.objects.filter(id=pk).update(**attendee)
        return JsonResponse(
            new_attendee, encoder=AttendeeDetailEncoder, safe=False
        )
