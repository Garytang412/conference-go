from common.json import ModelEncoder
from .models import Attendee, ConferenceVO, AccountVO


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]

    def get_extra_data(self, obj):
        return {"href": obj.get_api_url()}


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = ["name", "email", "company_name", "created", "conference"]
    encoders = {
        "conference": ConferenceVODetailEncoder()  # 原来是ConfferenceListEncoder
    }

    def get_extra_data(self, obj):
        count = AccountVO.objects.filter(email=obj.email)
        return {"has_account": len(count) > 0}
