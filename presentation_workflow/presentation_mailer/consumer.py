import json
import pika
import django
import os
import sys
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError
import time

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


while True:
    try:

        def process_approve_message(ch, method, properties, body):
            content = json.loads(body)
            # print(content)
            return send_mail(
                "Presentation Approved",
                "your presentation is approved:" + content["presenter_name"],
                "admin@conference.go",
                [content["presenter_email"]],
                fail_silently=False,
            )

        def process_reject_message(ch, method, properties, body):
            content = json.loads(body)
            # print(content)
            return send_mail(
                "Presentation Rejected",
                "your presentation is rejected:" + content["presenter_name"],
                "admin@conference.go",
                [content["presenter_email"]],
                fail_silently=False,
            )

        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()

        channel.queue_declare(queue="presentation_approve")
        channel.basic_consume(
            queue="presentation_approve",
            on_message_callback=process_approve_message,
            auto_ack=True,
        )

        channel.queue_declare(queue="presentation_reject")
        channel.basic_consume(
            queue="presentation_reject",
            on_message_callback=process_reject_message,
            auto_ack=True,
        )

        channel.start_consuming()
    except AMQPConnectionError:
        print("could not conneted to rabbitmq")
        time.sleep(2)
