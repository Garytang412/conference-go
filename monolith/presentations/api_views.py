from django.http import JsonResponse
from presentations.encoders import (
    PresentationListEncoder,
    PresentationDetailEncoder,
)
from .models import Presentation
from django.views.decorators.http import require_http_methods
import json
from events.models import Conference
from .producers import presentation_producer
import pika


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.filter(conference=conference_id)
        return JsonResponse(
            {"presentations": presentations},
            encoder=PresentationListEncoder,
            safe=False,
        )
    else:
        # post:
        new_pressentation = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            new_pressentation["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Conference ID"}, status=400
            )
        pressentation = Presentation.create(**new_pressentation)
        return JsonResponse(
            pressentation, encoder=PresentationDetailEncoder, safe=False
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_presentation(request, pk):
    if request.method == "GET":
        p = Presentation.objects.get(id=pk)
        return JsonResponse(p, encoder=PresentationDetailEncoder, safe=False)

    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        # PUT -> update
        content = json.loads(request.body)
        try:
            content["conference"] = Conference.objects.get(
                id=content["conference"]
            )
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Conference ID"}, status=400
            )
        Presentation.objects.filter(id=pk).update(**content)
        pressentation = Presentation.objects.get(id=pk)
        print(type(pressentation))
        return JsonResponse(
            pressentation, encoder=PresentationDetailEncoder, safe=False
        )


@require_http_methods(["PUT"])
def api_approve_presentation(request, pk):
    presentation = Presentation.objects.get(id=pk)
    presentation.approve()
    presentation_producer(
        presentation, "presentation_approve"
    )  # -> presentation_approve

    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_reject_presentation(request, pk):
    presentation = Presentation.objects.get(id=pk)
    presentation.reject()
    presentation_producer(
        presentation, "presentation_reject"
    )  # -> presentation_reject
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
