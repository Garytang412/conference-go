from common.json import ModelEncoder
from .models import Presentation
from events.encoders import ConferenceListEncoder


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
    ]
    encoders = {"conference": ConferenceListEncoder()}

    def get_extra_data(self, obj):
        return {"Status": obj.status.name}


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["title"]

    def get_extra_data(self, obj):
        return {"status": obj.status.name, "href": obj.get_api_url()}
