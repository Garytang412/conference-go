from events.encoders import (
    ConferenceListEncoder,
    ConferenceDetailEncoder,
    LocationListEncoder,
    LocationDetailEncoder,
)
from django.http import JsonResponse

from .models import Conference, Location, State
from django.views.decorators.http import require_http_methods
import json
from .acls import get_img_url, get_weather

###########################################################


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
            safe=False,
        )
    else:
        # POST:
        new_conference = json.loads(request.body)
        try:
            location = Location.objects.get(id=new_conference["location"])
            new_conference["location"] = location
        except Location.DoesNotExist:
            return JsonResponse({"message": "Location ID Invalid"}, status=400)

        conference = Conference.objects.create(**new_conference)

        return JsonResponse(
            conference, encoder=ConferenceDetailEncoder, safe=False
        )


@require_http_methods(["GET", "PUT"])
def api_show_conference(request, pk):
    if request.method == "GET":
        conference = Conference.objects.get(id=pk)
        weather = get_weather(
            conference.location.city, conference.location.state.name
        )

        return JsonResponse(
            {"conference": conference, "weather": weather},
            encoder=ConferenceDetailEncoder,
            safe=False,
        )

    elif request.method == "PUT":
        new_conference = json.loads(request.body)
        try:
            location = Location.objects.get(id=new_conference["location"])
            new_conference["location"] = location
        except Location.DoesNotExist:
            return JsonResponse({"message": "Location ID Invalid"}, status=400)

        Conference.objects.filter(id=pk).update(**new_conference)
        conference = Conference.objects.filter(id=pk)
        return JsonResponse(
            conference, encoder=ConferenceDetailEncoder, safe=False
        )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(locations, encoder=LocationListEncoder, safe=False)
    else:
        # POST
        content = json.loads(request.body)
        # Location.state is a ForeignKey. twoways to set it.
        # 1 print(content["state"]) ==> 'TX' use it to get the state
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
            content.update(get_img_url(content["city"], content["state"]))
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"}, status=400
            )
        # 2?

        location = Location.objects.create(**content)
        return JsonResponse(
            location, encoder=LocationDetailEncoder, safe=False
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_location(request, pk):
    if request.method == "GET":
        location = Location.objects.get(id=pk)
        return JsonResponse(
            location, encoder=LocationDetailEncoder, safe=False
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        # PUT --> update:
        new_location = json.loads(request.body)
        try:
            if "state" in new_location:
                new_state = State.objects.get(
                    abbreviation=new_location["state"]
                )
                new_location["state"] = new_state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"}, status=400
            )

        Location.objects.filter(id=pk).update(**new_location)
        location = Location.objects.get(id=pk)
        return JsonResponse(
            location, encoder=LocationDetailEncoder, safe=False
        )
