from common.json import ModelEncoder
from events.models import Location,Conference

class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ['name','href']


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ['name','href']

class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = ["name","city","room_count","created","updated","img_url"]

    def get_extra_data(self, obj):
        return {"state":obj.state.abbreviation}

class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = ["name",
        "starts",
        "ends",
        "description",
        "created",
        "updated",
        "max_presentations",
        "max_attendees",
        "location",
        ]
    encoders = {
        "location": LocationListEncoder(),
    }

