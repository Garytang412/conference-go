from django.urls import path

from .api_views import (
    api_list_conferences,
    api_list_locations,
    api_show_conference,
    api_show_location,
)


urlpatterns = [
    path(
        "conferences/", api_list_conferences, name="api_list_conferences"
    ),  # GET list, POST new conference
    path(
        "conferences/<int:pk>/",
        api_show_conference,
        name="api_show_conference",
    ),  # GET detail, PUT change, DELETE
    path(
        "locations/", api_list_locations, name="api_list_locations"
    ),  #  GET list, POST new
    path(
        "locations/<int:pk>/", api_show_location, name="api_show_location"
    ),  # GET detail, PUT change, Delete
]
