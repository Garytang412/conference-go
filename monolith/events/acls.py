from .keys import PEXELKS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_img_url(city, state):
    url = "https://api.pexels.com/v1/search"
    params = {"query": f"{city} {state}", "per_page": 1}
    headers = {"Authorization": PEXELKS_API_KEY}
    r = json.loads(requests.get(url, params=params, headers=headers).content)
    return {"img_url": r["photos"][0]["src"]["original"]}


def get_weather(city, state):
    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {"q": f"{city},{state},USA", "appid": OPEN_WEATHER_API_KEY}
    r = json.loads(requests.get(url, params=params).content)
    temp = (r["main"]["temp"] - 273.15) * 9 / 5 + 32
    return {
        "temp": temp,
        "description": r["weather"][0]["description"],
    }
